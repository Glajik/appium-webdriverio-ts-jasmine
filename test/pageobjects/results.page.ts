import Search from './search.component';

class Item {
  private parent: WebdriverIO.Element;

  constructor(element: WebdriverIO.Element) {
    this.parent = element;
  }

  get header () {
    return this.parent.$('div')
  }

  get body () {
    return this.parent.$('p')
  }

  get tags () {
    return this.parent.$$('ul > li')
  }

  get footer () {
    return this.parent.$('div:last')
  }

  get headerAncor() {
    return this.parent.$('div a')
  }

  get headerText() {
    return this.parent.$('div h3')
  }

  get isExactMath() {
    return this.parent.$('span=exact match')
  }
}

class Results extends Search {
  /**
   * define selectors
   */

  get elementMain () {
    return $('#main');
  }

  get headerFoundCount () {
    return this.elementMain.$('div h2')
  }

  get items () {
    return this.elementMain.$('div').$$('section');
  }

  open (packageName: string) {
    return super.open(`search?q=${packageName}`);
  }
}

export default new Results()
