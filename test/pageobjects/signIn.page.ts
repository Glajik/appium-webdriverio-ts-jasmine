import Page from './page';

class SignInPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputUsername () {
        return $('#login_username')
    }

    get inputPassword () {
        return $('#login_password')
    }

    get btnSubmit () {
        return $('button[type="submit"]')
    }

    get ancorHomepage () {
        return $('a[aria-label="Homepage"]')
    }

    get ancorCreateAccount () {
        return $('=Create Account')
    }

    get ancorForgotPassword() {
        return $('=Forgot password?')
    }

    get notificationPopupText() {
        return $('ul[aria-live="polite"] p:first-child')
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    login (username: string, password: string) {
        this.inputUsername.setValue(username);
        this.inputPassword.setValue(password);
        this.btnSubmit.click(); 
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open('login');
    }
}

export default new SignInPage();
