import Page from './page';

export default class SearchComponent extends Page {
  /**
   * define selectors
   */

  get formSearch () {
    return $('#search')
  }

  get inputSearch () {
    return this.formSearch.$('input[type="search"]')
  }

  get buttonSubmit () {
    return this.formSearch.$('button[type="submit"]')
  }

  search(packageName: string) {
    this.inputSearch.setValue(packageName);
    this.buttonSubmit.click();
  }
}