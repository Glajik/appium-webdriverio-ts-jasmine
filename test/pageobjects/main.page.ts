import Search from './search.component';

class MainPage extends Search {
  /**
   * define selectors
   */

  open() {
    return super.open('/');
  }
}

export default new MainPage()
