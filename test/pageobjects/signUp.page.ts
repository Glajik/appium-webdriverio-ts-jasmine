import Page from './page';

class SignUpPage extends Page {
  /**
   * define selectors
   */

  get inputUsername () {
    return $('#signup_name')
  }

  get inputEmail () {
    return $('#signup_email')
  }

  get inputPassword () {
    return $('#signup_password')
  }

  get checkboxAgree() {
    return $('#signup_eula-agreement')
  }

  get btnSubmit () {
    return $('button[type="submit"]')
  }

  get ancorHomepage () {
    return $('a[aria-label="Homepage"]')
  }
  
  get ancorLearnMore () {
    return $('=Learn more')
  }

  get ancorEndUserLicense () {
    return $('=End User License Agreement')
  }

  get ancorPrivacyPolicy () {
    return $('=Privacy Policy')
  }

  get ancorLogin () {
    return $('=or, Login')
  }

  get labelPleaseEnterValidEmail () {
    return $('label=Please enter a valid email')
  }

  get labelPleaseEnterValidPassword () {
    return $('label=Please enter a valid password that is at least 10 characters')
  }

  get elementPleaseCheckAgreeBox () {
    return $('p=Please check this box');
  }

  // Please check this box

  open () {
    return super.open('signup');
  }

  fillForm (username: string, email: string, password: string) {
    this.inputUsername.setValue(username);
    this.inputEmail.setValue(email);
    this.inputPassword.setValue(password);
    this.checkboxAgree.click();
  }

  signup (username: string, email: string, password: string) {
    this.fillForm(username, email, password);
    this.btnSubmit.click(); 
  }
}

export default new SignUpPage();
