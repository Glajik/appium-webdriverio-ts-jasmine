/// <reference types="expect-webdriverio"/>

import SignInPage from  '../pageobjects/signIn.page';
import SignUpPage from  '../pageobjects/signUp.page';
import MainPage from '../pageobjects/main.page';
import ResultsPage from '../pageobjects/results.page';
import config from '../../config';

const { host } = config;
const withBase = (path: string) => `${host}${path}`;

describe('Sign In to npmjs.com', () => {
    beforeAll(() => {
        SignInPage.open();
        browser.pause(1000);
    })

    it ('all page controls should be exist', () => {
        expect(SignInPage.inputUsername).toBeExisting();
        expect(SignInPage.inputPassword).toBeExisting();
        expect(SignInPage.btnSubmit).toBeExisting();
        expect(SignInPage.ancorCreateAccount).toBeExisting();
        expect(SignInPage.ancorForgotPassword).toBeExisting();
        // Notification popup should be hidden
        expect(SignInPage.notificationPopupText).not.toBeExisting();
    });

    it ('all ancors should be valid', () => {
        expect(SignInPage.ancorHomepage.getAttribute('href')).toBe(withBase('/'));
        expect(SignInPage.ancorCreateAccount.getAttribute('href')).toBe(withBase('/signup'));
        expect(SignInPage.ancorForgotPassword.getAttribute('href')).toBe(withBase('/forgot'));
    });

    it('should not login with invalid credentials', () => {
        SignInPage.login('anyuser', 'SuperSecretPassword!');

        expect(SignInPage.notificationPopupText).toBeExisting();
        expect(SignInPage.notificationPopupText).toHaveTextContaining(
            'username or password was invalid');
    });

    xit('should login with valid credentials', () => {
        SignInPage.login(config.username, config.password);
    
        // TODO:
        // Check page after sign in
    });
});


describe('Sign Up page', () => {
    beforeAll(() => {
        SignUpPage.open();
        browser.pause(1000);
    })

    it ('all page controls should be exist', () => {
        expect(SignUpPage.inputUsername).toBeExisting();
        expect(SignUpPage.inputEmail).toBeExisting();
        expect(SignUpPage.inputPassword).toBeExisting();
        expect(SignUpPage.checkboxAgree).toBeExisting();
        expect(SignUpPage.btnSubmit).toBeExisting();
        expect(SignUpPage.ancorLearnMore).toBeExisting();
        expect(SignUpPage.ancorEndUserLicense).toBeExisting();
        expect(SignUpPage.ancorPrivacyPolicy).toBeExisting();
        expect(SignUpPage.ancorLogin).toBeExisting();

        // Validation messages should be hidden
        expect(SignUpPage.labelPleaseEnterValidEmail).not.toBeExisting();
        expect(SignUpPage.labelPleaseEnterValidPassword).not.toBeExisting();
        expect(SignUpPage.elementPleaseCheckAgreeBox).not.toBeExisting();
    });

    it ('all ancors should be valid', () => {
        expect(SignUpPage.ancorLearnMore.getAttribute('href')).toBe('https://docs.npmjs.com/creating-a-strong-password');
        expect(SignUpPage.ancorEndUserLicense.getAttribute('href')).toBe(withBase('/policies/terms'));
        expect(SignUpPage.ancorPrivacyPolicy.getAttribute('href')).toBe(withBase('/policies/privacy'));
        expect(SignUpPage.ancorLogin.getAttribute('href')).toBe(withBase('/login'));
    });

    it('validation messages should be showed and submit button should be disabled', () => {
        const username = 'a';
        const email = 'some';
        const password = 'zzz';

        const urlBefore = browser.getUrl();

        SignUpPage.fillForm(username, email, password);

        // We should not to be redirected
        expect(browser.getUrl()).toBe(urlBefore);

        expect(SignUpPage.labelPleaseEnterValidEmail).toBeExisting();
        expect(SignUpPage.labelPleaseEnterValidPassword).toBeExisting();
        expect(SignUpPage.btnSubmit).not.toBeEnabled();
    });

    it('validation of checkbox "Agree to end user license..." should work', () => {
        // Reload page
        browser.refresh();

        // Check box
        SignUpPage.checkboxAgree.click();
        
        // Uncheck box - we should see validation message
        SignUpPage.checkboxAgree.click();
        expect(SignUpPage.elementPleaseCheckAgreeBox).toBeExisting();
        expect(SignUpPage.btnSubmit).not.toBeEnabled();
        
        // Check box again - message should be hidden
        SignUpPage.checkboxAgree.click();
        expect(SignUpPage.elementPleaseCheckAgreeBox).not.toBeExisting();
        expect(SignUpPage.btnSubmit).toBeEnabled();
    })

    xit('should create account', () => {
        const { username, email, password } = config;

        SignUpPage.signup(username, email, password);

        // TODO:
        // Check page after sign in
    });

    describe('Searching packages', () => {
        beforeAll(() => {
            MainPage.open();
            browser.pause(1000);
        })

        it('Should find expect-webdriverio package', () => {
            MainPage.inputSearch.setValue('expect-webdriverio');
            MainPage.buttonSubmit.click();

            expect(ResultsPage.headerFoundCount).toHaveText('2 packages found');
        });
    });
});