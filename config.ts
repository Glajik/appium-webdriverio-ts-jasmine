import env from  'dotenv';

env.config();

const username = process.env.USERNAME;
const email = process.env.EMAIL;
const password = process.env.PASSWORD;
const host = process.env.HOST;

if (!username) {
  throw new Error("Key 'USERNAME' not found in .env file");
}
if (!email) {
  throw new Error("Key 'EMAIL' not found in .env file");
}
if (!password) {
  throw new Error("Key 'PASSWORD' not found in .env file");
}
if (!host) {
  throw new Error("Key 'HOST' not found in .env file");
}

export default { username, email, password, host };
